package com.tedu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StMallApplication {

	public static void main(String[] args) {
		SpringApplication.run(StMallApplication.class, args);
	}

}
